ManPagesTextFile
"MakeSSTier" "jeanphilippegoldman" 20191104 0
<entry> "What is the Speech Segements Tier?"
<normal> "ProsoBox is designed for extracting prosodic measures at three levels: syllables (at a micro level), speech segments (ss) (at a macro level) and sound file (at a global level). The macro level of segmentation is called speech segments and is left to user's discretion. Depending on user's needs and objectives, speech segments can equate to interpausal units, utterances, sentences, turns at talk or other relevant linguistic units."
<normal> "The SS tier must provide a macro segmentation. By default, the SS tier is the #ortho tier with an orthographic transcription (from EasyAlign). The user can specify any other speech segments tier, with any name."
<entry> "How to fill in the form: Make SS tier?"
<normal> "The script %%Make SS tier% automatically creates a tier with interpausal units, in case you do not have any tier for speech segments. Indicate the path to one or more TextGrids and fill in the form:"
<list_item> "##pause threshold (in seconds)#: The script segments the sound file based on silent pauses longer than 0.250 seconds (by default). Modify the silent pause threshold (expressed in seconds) according to your needs."
<list_item> "##syll tier#: Specify the name of the tier where syllabic segmentation can be found (by default: syll)."
<list_item> "##ss tier#: Specify the name of the new speech segment tier (by default: ss)." 
<list_item> "##overwrite ss tier#: Check if you want to replace an existing ss tier."
<list_item> "##Pause character#: Indicate the symbol for labelling silent pause intervals (bu default: _)."
<entry> "What do I get after this step is completed?"
<normal> "The script modifies the TextGrid by inserting a new tier with speech segments, called ss (or any other specified name). You'd better check whether the new tier corresponds to your needs. You can fill in the intervals with orthographic transcription or annotation (e.g. sentence 1, sentence 2, etc.). Those labels will appear later in the SpeechSegment Table."
<normal> "Go to next step: @@ProsoProm@"