ManPagesTextFile
"ProsoProm" "jeanphilippegoldman" 20191104 0

<entry> "What is ProsoProm?"
<normal> "ProsoProm is a script that automatically detects prominent syllables based on acoustic cues. It adds a tier called #promauto in the TextGrid and a new column in the Table of syllables (SyllSheet). It also creates a new point tier called midvowel, pointing to the middle of each vowel."
<normal> "The algorithm takes acoustic features of the syllable into account, namely the relative length, relative pitch and internal pitch movements. Each syllable is compared to surrounding syllables (scope by default: 2 syllables before and 1 syllable after) and is detected as prominent when longer (relative duration = 2), higher (+ 2 semi-tones) or dynamic (internal pitch movement > 2.5 semi-tones). The scope and thresholds are modifiable by the user via the submenu @@ProsoPromThresholds@."
<normal> "The algorithm has been trained on spoken French (see References cited below)."

<entry> "How to fill in the form?"
<normal> "##Input Files#"
<normal> "The script will use the syll-sheet.txt file(s) correponding to the sound file(s). The script can process a collection of sound files (use * symbol)."

<normal> "##Prom Parameters#"
<list_item> "\bu ##F0 param#: Select the method for calculating relative pitch of the syllable. Options: #Mean = mean pitch value on the syllable nucleus. #Start = pitch value at the beginning of the syllabic nucleus. #End = pitch value at the end of the syllabic nucleus. #Min = minimal pitch value on the syllabic nucleus. #Max = maximu pitch value on the syllabic nucelus."
<list_item> "\bu ##Block pause (s)#: Indicate pause duration (in seconds) for blocking analysis. Whether a pause precedes or follows the target syllable, preceding or following syllables are excluded from the scope (window of analysis for relative parameters)."
<list_item> "\bu ##Weigth with distance# Check the box to apply lower weight to distant syllables within scope, i.e. the further the syllable from the considered syllable, the less it will influence the prominence score."
<list_item> "\bu #Scope: Define left and right scope for relative parameters analysis. Default value sets a scope of 2 syllables before  and 1 syllable after the target syllable."
<list_item> "\bu #Strategy: The script has 2 options for prominence detection: a binary detection (prominent syllabe P vs non prominent syllable) or a continuous prominence score (ranging from non prominent syllable 0 to highly prominent syllable 4). TO-CHECK. Opt for a binary (threshold) or gradual (cumulative) detection of prominent syllable. When the #threshold option is selected, a syllable is detected as prominent as soon as one prosodic parameter is above the threshold. When the #gradual (cumulative) option is selected, a score of prominence ranging from 0 to 4 is attributed according the number of parameters."

<normal> "More information for customizing the values of the thresholds: see submenu @@ProsoPromThresholds@"

<normal> "##Prom Annotation#"
<normal> "The script allows for a comparison of the automatic detection (tier %promauto) with another annotation tier provided by the user (for example, a manually added %prommanu tier). See @@ProsoPromAnnotation@ for more detail." 
<list_item> "\bu #Prom tier: Encode the name of the tier with an alternative prominence annotation tier. Beware that both tier must have the same boundaries and number of syllabic intervals."
<list_item> "\bu ##Do prom eval#: The script compares 2 syllable tiers annotated for prominence. The comparison score is displayed within the Info Praat window."
<list_item> "\bu ##Add relative parameters tiers#: The script adds three tiers at the bottom of the TextGrid with relatives parameters calculated for each syllable: ##f0 rel# returns the difference in pitch of the target syllable as compared to surrounding syllables (in semi-tones); #durrel returns the relative length of the target syllable (2 = twice as long as the mean length of surrounding syllables); #mvt returns the positives pitch movement (if any) of the target syllable (in semi-tones). For each parameter, the symbol \#  signals the relevant parameter as for prominence detection."

<normal> "##Speech Segment Annotation#"
<normal> "The script calculates the proportion of prominent syllables per speech segment (SS)." 
<list_item> "\bu #SS tier: Encode the name of the speech segment (ss) tier in the TextGrid (by default: ortho)."
<normal> "##Write in Praat Info Window#"
<list_item> "\bu ##Verbose#: Select #3.Full or #2.Short or #1.None to display more or less info"

<entry> "What do I get after this step is completed?"
<normal> "ProsoProm modifies the existing TextGrid by inserting a new tier called promauto. The script also modifies the Table of Syllables (FILE-NAME_syllsheet.txt) by inserting information about prominence detection. It possibly adds more tiers if you perform a comparison of various prominence tiers. "
<normal> "Go to next step: @@MakeSSTable@"
<entry> "References"
<normal> "ProsoProm has been trained on the manually annotated C-PROM corpus of spoken French (DOI: 10.26037/yareta:2jqreak2hrexdirkjfyp4gq2gq)."
<list_item> "\bu Simon, A.C., Avanzi, M., Goldman, J.-P. 2008. ##La détection des proéminences syllabiques. Un aller-retour entre l'annotation manuelle et le traitement automatique.# %%Actes du Congrès Mondial de Linguistique Française 2008%, p. 1685-1698. doi:10.1051/cmlf08256"
<list_item> "\bu Goldman, J.-P., Auchlin, A., Roekhaut, S., Simon, A.C., Avanzi, M. 2010. ##Prominence perception and accent detection in French. A corpus-based account.# %%Proceedings Speech Prosody 2010 (Chicago, 11-14 mai 2010)%."
<list_item> "\bu Goldman, J.-P., Avanzi, M., Auchlin, A., Simon, A.C. 2012. ##A Continuous Prominence Score Based on Acoustic Features#. %%Proceedings of InterSpeech 2012% Portland, p. 2454-2457."