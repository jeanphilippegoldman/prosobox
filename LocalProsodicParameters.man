ManPagesTextFile
"LocalProsodicParameters" "jeanphilippegoldman" 20191104 0
<entry> "The Local Prosodic Parameters in the Table of Syllables: Where they are used"
<normal> "The script ProsoGram creates 1 Table of Syllables with local prosodic measures, that is measures of syllable duration, syllable f0, syllabic prominence, etc. This table is modified by the script ProsoProm."
<normal> "UNDER CONSTRUCTION This page describes the local parameters that are used for prominence detection by @@ProsoProm@ and by @@ProsoReport@."

<entry> "Local Prosodic Parameters: List and Definitions"
<list_item> "\bu ##file#: File name"
<list_item> "\bu ##xxx#: "
<list_item> "\bu ##xxx#: ."
<list_item> "\bu ##xxx#: ."


<normal> "If: ProsoProm has been done @@ProsoProm@"
<list_item> "\bu #xxx: "
<list_item> "\bu #xxx: "
<list_item> "\bu #xxx: "	
<list_item> "\bu #xxx: "

