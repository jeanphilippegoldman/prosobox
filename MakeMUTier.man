ManPagesTextFile
"MakeMUTier" "jeanphilippegoldman" 20190617 0
<entry> "What is MU Tier?"
<normal> "ProsoBox is designed for extracting prosodic measures at three levels: syllbales (micro), utterances (macro), sound file (global). The macro-unit level (MU) of segmentation is left to user's discretion. Depending on user's needs and objectives, macro-units can equate to sentences, turns at talk or other relevant linguistic units."
<normal> "The MU tier must provide a macro segmentation. By default, the MU tier is the #ortho tier with an orthographic transcription (from EasyAlign). The user can specify any other macro-unit tier, with any name."
<entry> "How to fill in the form: Make MU tier?"
<normal> "The script %%Make MU tier% automatically creates a tier with interpausal units, in case you do not have any macro-unit tier. Select a TextGrid and fill in the form:"
<list_item> "##pause threshold (in seconds)#: The script segments the sound file based on silent pauses longer than 0.250 seconds (by default). Modify the silent pause threshold expressed in seconds according to your needs."
<list_item> "##syll tier#: Specify the name of the tier where syllabic segmentation can be found (by default: syll)."
<list_item> "##mu tier#: Specify the name of the new macro-unit tier (by default: mu)." 
<entry> "What do I get after this step is completed?"
<normal> "The script modifies the TextGrid by inserting a new macro-unit tier called mu (or any other name, if specified). You'd better check whether the new tier corresponds to your needs. You can fill in the intervals with orthographic transcription or annotation (e.g. sentence 1, sentence 2, etc.). Those labels will appear later in the macro-unit Table."
<normal> "Go to next step: @@ProsoProm@"