ManPagesTextFile
"MakeSWTable" "jeanphilippegoldman" 20191104 0
<entry> "What is the script %%Make SW Table% for?"
<normal> "The abreviation SW stands for %%Sliding Window%. It refers to a fixed-size sequence of syllables (a window) used for calculating macro prosodic parameters step by step (sliding) through a sound file. The script %%Make SW Table% creates a table of fixed-size Units (%FILE-NAME_sw.txt) with a range of acoustic measures for each unit."
<normal> "The Table of SW is used for dynamic display of macroprosody within the @@ProsoDyn@ panel."

<entry> "How to fill in the form?"
<normal> "##win length#: Length of the window, that is the number of syllables in the window (integer). By default: 15 syllables."
<normal> "##win step#: Size of the step, that is the number of syllables the window moves forward. By default: 1 syllable."
<normal> "##win strength#: TO-CHECK"
<normal> "##do delta params#: Add 4 additional columns calculating the difference between a window (eg. 15 syllables) and the previous one. This allows for identifying dynamic prosodic modifications, like speech rate increase/decrease or f0 range upstep/downstep."
<normal> "##quiet#: uncheck if you want to print info in the Praat Info window (does not impact on the result)"

<entry> "What do I get after this step is done?"
<normal> "The script creates a new Sliding Window Table (FILE-NAME_sw.txt) with prosodic measures."
<normal> "See @@GlobalProsodicParameters@ for a complete list and definition of measures displayed in the Table."
<normal> "Go to next step: @@ProsoDyn@ or @@ProsoReport@ or @@Replot@ Enriched Prosogram"